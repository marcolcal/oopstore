<?php 

class Transaction{
	private $id
	private $value;
	private $amount;
	private $date;
	private $status;


	public static $TRANSACTION_STATUS_APPROVED = 1;
	public static $TRANSACTION_STATUS_DECLINED = 1;
	public static $TRANSACTION_STATUSES = array(
		1 => 'Approved',
		2 => 'Declined',
		3 => 'Pending',
		4 => 'Canceled'
	);
	
}