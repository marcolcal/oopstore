<?php

class employee extends human{
	private $salary;

	public function __construct($name, $age, $salary){
		parent::__construct($name,$age);
		$this->salary = $salary;
	}

	public function setSalary($salary){
		$this->salary = $salary;
	}

	public function getSalary(){
		return $this->salary;
	}

	public function display(){
		parent::display();
		echo 'Salary: '.$this->salary;
		echo '<br>';
	}
}