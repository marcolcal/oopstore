<?php 

class topEmployee extends employee{
	private $rank;

	function __contruct($name, $age, $salary,$rank){
		parent::__construct($name, $age, $salary);
		$this->rank=$rank;

	}

	public function setRank($rank){
		$this->rank=$rank;
	}

	public function getRank(){
		return $this->rank;
	}

	public function display(){

		parent::display();
		echo 'Rank:'.$this->rank;
		echo'<br>';
		human::sing();
	}
}