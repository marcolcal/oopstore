<?php 
class Product{
	private $name;
	private $description;
	private $price;
	private $image;
	private $onSaleStatus;

	function __construct($name = "",$description = "",$price ="",$image=""){
		$this->name = $name;
		$this->description = $description;
		$this->price = $price;
		$this->image = $image;
		$this->onSaleStatus = $onsale;
	}

	function setName($name){ //Setter
		$this->name = $name;
	}

	function getName(){  //Getter
		return $this->name;
	}

	function setDescription($description = 10){
		$this->description=$description;
	}

	function getDescription(){
		return $this->description;
	}

	function setPrice($price){
		$this->price=$price;
	}

	function getPrice(){
		return $this->price;
	}

	function setImage($image){
		$this->image=$image;
	}

	function getImage(){
		return $this->image;
	}


	function showProduct(){
		echo '<div class="product">';
		echo '<h1>'.$this->name.'</h1>';
		echo '<p>'.$this->description.'</p>';
		echo '<h6>$'.$this->price.'</h6>';
		echo '</div>';
	}


}





// Class is like a blueprint (Mold)

// mold with shape of hearth (class)
// new candle (object)
// seond candle(object)
// third candle(object)

?>